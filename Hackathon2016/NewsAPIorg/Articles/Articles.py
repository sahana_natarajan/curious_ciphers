import requests

def printHorizontalLine(style="-", length=50):
    for _ in xrange(length):
        print style,
    print ""

def get_articles_by_source(source="financial-times"):
    parameters = {"sortBy": "top", "source": source}
    print "SOURCE:\n", source
    url = "https://newsapi.org/v1/articles?apiKey=5012b4df9eb24c719a7170569a0b4729"
    response = requests.get(url, parameters)
    jsonData = response.json()
    print jsonData
    for article in jsonData["articles"]:
        print article["title"]

file = open("C:\\Users\\rnachia\\Documents\\Hackathon_2016_files\\Hackathon2016\\NewsAPIorg\\Categories\\business.txt","r")
for line in file.readlines():
    source = line.rstrip()
    get_articles_by_source(source)
    printHorizontalLine()

get_articles_by_source()