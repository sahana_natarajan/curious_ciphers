import requests

def printHorizontalLine(style="-", length=50):
    for _ in xrange(length):
        print style,
    print ""

def get_sources_by_category(category="business"):
    parameters = {"category": category, "language": "en", "country": ""}
    print "CATEGORY:\n", category
    print "\nSOURCES:"
    url = "https://newsapi.org/v1/sources?apiKey=5012b4df9eb24c719a7170569a0b4729"
    response = requests.get(url, parameters)
    jsonData = response.json()
    file = open(category + ".txt", "w")
    for source in jsonData["sources"]:
        print source["id"]
        file.write(source["id"])
        file.write("\n")
    file.close()
    printHorizontalLine()

categories = ["business", "entertainment", "gaming", "general", "music", "science-and-nature", "sport", "technology"]
for category in categories:
    get_sources_by_category(category)
