from __future__ import division
import pandas as pd
import re as regex
import numpy as np
import itertools
from nltk.corpus import stopwords  # Import the stop word list
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.ensemble import RandomForestClassifier


def clean_news_to_words(lead_paragraph):
    review_text = str(lead_paragraph)
    letters_only = regex.sub("[^a-zA-Z]", " ", review_text)
    words = letters_only.lower().split()
    stops = set(stopwords.words("english"))
    meaningful_words = [w for w in words if not w in stops]
    return (" ".join(meaningful_words))


def calculate_accuracy(actual_domain, predicted_domain):
    correctPrediction = 0
    for i in xrange(0, len(actual_domain)):
        print actual_domain[i], predicted_domain[i]
        if (actual_domain[i] == predicted_domain[i]):
            correctPrediction += 1
    print correctPrediction
    accuracy = (correctPrediction / len(actual_domain))
    print "Accuracy=", accuracy


def level_classification(training_file, test_file, feature):
    train = pd.read_csv(training_file, header=0)
    news = train["Snippet"]
    news_major_keywords = train["Major Keywords"]
    news_minor_keywords = train["Minor Keywords"]

    # print stopwords.words("english")
    # clean_news = clean_news_to_words(news[0])
    # print clean_news

    # Initialize an empty list to hold the clean reviews
    clean_news_train = []

    for nw, nw_major, nw_minor in itertools.izip(news, news_major_keywords, news_minor_keywords):
        """print 'news:', nw
        print 'major: ', nw_major
        print 'minor: ', nw_minor
        """
        clean_news_train.append(clean_news_to_words(nw) + clean_news_to_words(nw_major) + clean_news_to_words(nw_minor))

    print "Creating the bag of words...\n"

    # Initialize the "CountVectorizer" object, which is scikit-learn's
    # bag of words tool.
    vectorizer = CountVectorizer(analyzer="word", \
                                 tokenizer=None, \
                                 preprocessor=None, \
                                 stop_words=None, \
                                 max_features=5000)

    train_data_features = vectorizer.fit_transform(clean_news_train)
    train_data_features = train_data_features.toarray()

    print train_data_features.shape
    print train_data_features.shape[0], "rows with ", train_data_features.shape[
        1], "features(one for each vocabulary word)"

    vocabulary = vectorizer.get_feature_names()
    # print vocabulary

    # Sum up the counts of each vocabulary word
    dist = np.sum(train_data_features, axis=0)

    # For each, print the vocabulary word and the number of times it
    # appears in the training set
    """
    for tag, count in zip(vocabulary, dist):
        print count, tag
    """

    print "Building Random Forests . . . "

    # Initialize a Random Forest classifier with 100 trees
    forest = RandomForestClassifier(n_estimators=100)

    # Fit the forest to the training set, using the bag of words as
    # features and the sentiment labels as the response variable
    #
    # This may take a few minutes to run
    forest = forest.fit(train_data_features, train[feature])

    # Read the test data
    test = pd.read_csv(test_file, header=0)

    print test.shape

    # Create an empty list and append the clean reviews one by one
    num_reviews = len(test["Snippet"])
    clean_news_test = []

    print "Cleaning and parsing the test set movie reviews...\n"

    news = test["Snippet"]
    news_major_keywords = test["Major Keywords"]
    news_minor_keywords = test["Minor Keywords"]

    for nw, nw_major, nw_minor in itertools.izip(news, news_major_keywords, news_minor_keywords):
        """print 'news:', nw
        print 'major: ', nw_major
        print 'minor: ', nw_minor
        """
        clean_news_test.append(clean_news_to_words(nw) + clean_news_to_words(nw_major) + clean_news_to_words(nw_minor))

    """for nw in test["Snippet"]:
        print clean_news_to_words(nw)
        clean_news_test.append(clean_news_to_words(nw))
    """

    # Get a bag of words for the test set, and convert to a numpy array
    test_data_features = vectorizer.transform(clean_news_test)
    test_data_features = test_data_features.toarray()

    # Use the random forest to make sentiment label predictions
    result = forest.predict(test_data_features)

    # Copy the results to a pandas dataframe with an "id" column and
    # a "sentiment" column

    output = pd.DataFrame(
        data={"Article Id": test["Article Id"], "Snippet": test["Snippet"],
              "Major": test["Products"],
              "Major Keywords": test["Major Keywords"],
              "Minor Keywords": test["Minor Keywords"],
              "Field": test["Field"], "Products": test["Products"]})

    # Use pandas to write the comma-separated output file
    file_name = "Test_Output_News_With_" + feature + ".csv"
    output.to_csv(file_name, index=False)
    calculate_accuracy(test[feature], result)
    return file_name


file_name = level_classification("../nyTimes/trimble_features_train.csv",
                                 "../nyTimes/trimble_features_test.csv", "Domain")
file_name = level_classification("../nyTimes/trimble_features_train.csv", file_name, "Field")
file_name = level_classification("../nyTimes/trimble_features_train.csv", file_name, "Products")
