# get some libraries that will be useful
import re
import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
# the Naive Bayes model
from sklearn.naive_bayes import MultinomialNB
# function to split the data for cross-validation
from sklearn.model_selection import train_test_split
# function for transforming documents into counts
from sklearn.feature_extraction.text import CountVectorizer
# function for encoding categories
from sklearn.preprocessing import LabelEncoder


def normalize_text(s):
    s = str(s)
    s = s.lower()

    # remove punctuation that is not word-internal (e.g., hyphens, apostrophes)
    s = re.sub('\s\W', ' ', s)
    s = re.sub('\W\s', ' ', s)

    # make sure we didn't introduce any double spaces
    s = re.sub('\s+', ' ', s)
    s = re.sub(r'[^\w]', '', s)
    return s


# grab the data
news = pd.read_csv("../nyTimes/agriculture_features_train.csv", header=0)
news.head()
news['TEXT'] = [normalize_text(s) for s in news['Snippet']]

news.head()

vectorizer = CountVectorizer()

x_train = vectorizer.fit_transform(news['TEXT'])

encoder = LabelEncoder()
y_train = encoder.fit_transform(news['Domain'])


# grab the data
news = pd.read_csv("../nyTimes/agriculture_features_test.csv", header=0)
news.head()
news['TEXT'] = [normalize_text(s) for s in news['Snippet']]

news.head()

vectorizer = CountVectorizer()

x_test = vectorizer.fit_transform(news['TEXT'])

encoder = LabelEncoder()
y_test = encoder.fit_transform(news['Domain'])

# split into train and test sets
# x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.20)

# take a look at the shape of each of these
print(x_train.shape)
print(y_train.shape)
print(x_test.shape)
print(y_test.shape)

nb = MultinomialNB()
result = nb.fit(x_train, y_train).predict(x_test)

print nb.score(x_test, y_test)

print result