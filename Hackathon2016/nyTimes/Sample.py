import requests
import requests.packages.urllib3
import csv
import json


def printHorizontalLine(style="-", length=50):
    for _ in xrange(length):
        print style,
    print ""


def get_news(filename="test.csv", q="", begin_date="", end_date="", page="0"):
    parameters = {"page": page, "q": q, "begin_date": begin_date, "end_date": end_date,
                  "api-key": "ef9e078eb870498db7a37ef9c245645a",
                  "fl": "snippet,headline,lead_paragraph,abstract,section_name,keywords,pub_date,_id"}
    url = "https://api.nytimes.com/svc/search/v2/articlesearch.json"
    response = requests.get(url, parameters)
    print response.content
    jsonData = response.json()
    docs = jsonData["response"]["docs"]
    extension = ".csv"
    mainFile = csv.writer(open(filename + "_classfication" + extension, 'ab+'))
    featureFile = csv.writer(open(filename + "_features" + extension, 'ab+'))
    if (page == 0):
        mainFile.writerow(["Article Id", "Snippet", "Lead Paragraph", "Abstract"])
        featureFile.writerow(
            ["Article Id", "Snippet", "Headline", "Headline kicker", "Section Name", "Published Date", "Major Keywords",
             "Minor Keywords"])
    for doc in docs:
        headline = checkValue(doc["headline"]["main"])
        headlineKicker = ""
        try:
            headlineKicker = doc["headline"]["kicker"]
            headlineKicker = headlineKicker.encode("utf-8")
        except KeyError:
            pass

        abstract = checkValue(doc["abstract"])
        leadPara = checkValue(doc["lead_paragraph"])
        snippet = checkValue(doc["snippet"])
        sectionName = checkValue(doc["section_name"])

        if leadPara == "":
            leadPara = snippet + abstract

        majorKeyword = ""
        minorKeyword = ""
        keywordValues = ""

        for keyword in doc["keywords"]:
            if keyword:
                if "is_major" not in keyword:
                    majorKeyword += keyword["value"] + ","
                elif keyword["is_major"] == "Y":
                    majorKeyword += keyword["value"] + ","
                elif keyword["is_major"] == "N":
                    minorKeyword += keyword["value"] + ","
                keywordValues += keyword["value"] + ","

        mainFile.writerow([doc["_id"].encode("utf-8"), snippet, leadPara, abstract])
        featureFile.writerow([doc["_id"].encode("utf-8"), snippet, headline, headlineKicker,
                              sectionName, doc["pub_date"].encode("utf-8"),
                              majorKeyword.encode("utf-8"), minorKeyword.encode("utf-8")])


def checkValue(value):
    if value is None:
        return ""
    else:
        return value.encode("utf-8")


print "Enter a file name:",
filename = raw_input()

print "Enter keywords:",
keywords = raw_input()

for page in range(3):
    get_news(filename, keywords, "20100101", "20161231", page)

# get_news(filename, keywords, "20100101", "20161231", 0)
